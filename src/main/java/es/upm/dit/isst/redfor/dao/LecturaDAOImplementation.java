package es.upm.dit.isst.redfor.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import es.upm.dit.isst.redfor.model.Lectura;

public class LecturaDAOImplementation implements LecturaDAO {

	private static LecturaDAOImplementation instancia = null;

	private LecturaDAOImplementation() {
	}

	public static LecturaDAOImplementation getInstance() {
		if (null == instancia)
			instancia = new LecturaDAOImplementation();
		return instancia;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Lectura> readLecturas(int id) {
		List<Lectura> lecturasTodas = new ArrayList<Lectura>();
		List<Lectura> lecturasNodo = new ArrayList<Lectura>();

		Session s = SessionFactoryService.get().openSession();
		s.beginTransaction();

		lecturasTodas.addAll(s.createQuery("FROM Lectura").list());

		for (Lectura lectura : lecturasTodas) {
			if (lectura.getIdNodo() == id) {
				lecturasNodo.add(lectura);
			}
		}

		s.getTransaction().commit();
		s.close();

		return lecturasNodo;
	}

}
