package es.upm.dit.isst.redfor.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import es.upm.dit.isst.redfor.model.Actualizacion;
import es.upm.dit.isst.redfor.model.Ticket;
import es.upm.dit.isst.redfor.dao.TicketDAOImplementation;
import es.upm.dit.isst.redfor.dao.ActualizacionDAOImplementation;

@Path("/tickets")
public class TicketsActualizacionesPaths {

	// tickets

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Ticket> readTickets() {
		return TicketDAOImplementation.getInstance().readTickets();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createTicket(Ticket tnew) throws URISyntaxException {
		Ticket t = TicketDAOImplementation.getInstance().createTicket(tnew);
		if (t != null) {
			System.out.println(t.toString());
			URI uri = new URI("/REDFOR-SERVICE/rest/tickets/" + t.getId());
			return Response.created(uri).build();
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response readTicket(@PathParam("id") int id) {
		Ticket t = TicketDAOImplementation.getInstance().readTicket(id);
		if (t == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		return Response.ok(t, MediaType.APPLICATION_JSON).build();
	}

	@POST
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTicket(@PathParam("id") int id, Ticket tNew) throws URISyntaxException {
		System.out.println("Update request for " + id + " " + tNew.toString());
		Ticket tOld = TicketDAOImplementation.getInstance().readTicket(id);
		if ((tOld == null) || (!(tOld.getId() == id))) {
			return Response.notModified().build();
		}
		TicketDAOImplementation.getInstance().updateTicket(tNew);
		return Response.ok().build();
	}

	// actualizaciones

	@GET
	@Path("{id}/actualizaciones")
	@Produces(MediaType.APPLICATION_JSON)
	public Response readActualizaciones(@PathParam("id") int id) {
		List<Actualizacion> a = ActualizacionDAOImplementation.getInstance().readActualizaciones(id);
		if (a == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		return Response.ok(a, MediaType.APPLICATION_JSON).build();
	}

	@POST
	@Path("{id}/actualizaciones")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createActualizacion(@PathParam("id") int id, Actualizacion anew) throws URISyntaxException {
		Actualizacion a = ActualizacionDAOImplementation.getInstance().createActualizacion(anew);
		if (a != null) {
			URI uri = new URI("/REDFOR-SERVICE/rest/tickets/" + id + "/actualizaciones/" + a.getId());
			return Response.created(uri).build();
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

}