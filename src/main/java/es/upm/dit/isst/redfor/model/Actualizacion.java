package es.upm.dit.isst.redfor.model;

import java.io.Serializable;

import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class Actualizacion implements Serializable {
	private static final long serialVersionUID = 1L;

	private String descripcion;
	@Lob
	private byte[] foto;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String emailAgenteAutor;
	@ManyToOne
	private Ticket ticket;
	private long timestamp;

	public Actualizacion() {
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmailAgenteAutor() {
		return emailAgenteAutor;
	}

	public void setEmailAgenteAutor(String emailAgenteAutor) {
		this.emailAgenteAutor = emailAgenteAutor;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Actualizacion other = (Actualizacion) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Actualizacion [descripcion=" + descripcion + ", foto=" + Arrays.toString(foto) + ", id=" + id
				+ ", emailAgenteAutor=" + emailAgenteAutor + ", ticket=" + ticket + ", timestamp=" + timestamp + "]";
	}

}