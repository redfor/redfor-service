package es.upm.dit.isst.redfor.dao;

import java.util.List;
import es.upm.dit.isst.redfor.model.Lectura;

public interface LecturaDAO {
	public List<Lectura> readLecturas(int id);
}
