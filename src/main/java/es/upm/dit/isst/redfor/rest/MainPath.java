package es.upm.dit.isst.redfor.rest;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;



@ApplicationPath("rest")
public class MainPath extends ResourceConfig {
	public MainPath() {
		packages("es.upm.dit.isst.redfor.rest");
	}
}