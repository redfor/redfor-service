package es.upm.dit.isst.redfor.dao;

import java.util.List;

import es.upm.dit.isst.redfor.model.Nodo;

public interface NodoDAO {
	public List<Nodo> readNodos();

	public Nodo createNodo(Nodo n);

	public Nodo readNodo(int id);
}
