package es.upm.dit.isst.redfor.dao;

import java.util.List;

import es.upm.dit.isst.redfor.model.Ticket;

public interface TicketDAO {

	public List<Ticket> readTickets();

	public Ticket readTicket(int id);

	public Ticket createTicket(Ticket t);

	public Ticket updateTicket(Ticket t);

}