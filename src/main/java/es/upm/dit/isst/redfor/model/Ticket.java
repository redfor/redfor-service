package es.upm.dit.isst.redfor.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import es.upm.dit.isst.redfor.enums.TipoTicket;
import es.upm.dit.isst.redfor.model.Nodo;

@Entity
public class Ticket implements Serializable {
	private static final long serialVersionUID = 1L;

	private boolean activo;
	private String descripcion;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String emailAgenteAutor;
	@ManyToOne
	private Nodo nodo;
	private long timestamp;
	private TipoTicket tipo;
	private String titulo;
	@ElementCollection(targetClass=String.class,fetch = FetchType.EAGER)
	private List<String> emailsAgentesAsignados;

	public Ticket() {
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}


	

	
	
	
	public List<String> getEmailsAgentesAsignados() {
		return emailsAgentesAsignados;
	}

	public void setEmailsAgentesAsignados(List<String> emailsAgentesAsignados) {
		this.emailsAgentesAsignados = emailsAgentesAsignados;
	}

	
	
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getEmailAgenteAutor() {
		return emailAgenteAutor;
	}

	public void setEmailAgenteAutor(String emailAgenteAutor) {
		this.emailAgenteAutor = emailAgenteAutor;
	}

	public Nodo getNodo() {
		return nodo;
	}

	public void setNodo(Nodo nodo) {
		this.nodo = nodo;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public TipoTicket getTipo() {
		return tipo;
	}

	public void setTipo(TipoTicket tipo) {
		this.tipo = tipo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ticket other = (Ticket) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Ticket [activo=" + activo + ", descripcion=" + descripcion + ", id=" + id + ", emailAgenteAutor="
				+ emailAgenteAutor + ", nodo=" + nodo + ", timestamp=" + timestamp + ", tipo=" + tipo + ", titulo="
				+ titulo + ", emailsAgentesAsignados=" + emailsAgentesAsignados + "]";
	}

}