package es.upm.dit.isst.redfor.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import es.upm.dit.isst.redfor.model.Actualizacion;

public class ActualizacionDAOImplementation implements ActualizacionDAO {

	private static ActualizacionDAOImplementation instancia = null;

	private ActualizacionDAOImplementation() {
	}

	public static ActualizacionDAOImplementation getInstance() {
		if (null == instancia)
			instancia = new ActualizacionDAOImplementation();
		return instancia;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Actualizacion> readActualizaciones(int id) {
		List<Actualizacion> actTodas = new ArrayList<Actualizacion>();
		List<Actualizacion> actTicket = new ArrayList<Actualizacion>();

		Session s = SessionFactoryService.get().openSession();
		s.beginTransaction();

		actTodas.addAll(s.createQuery("FROM Actualizacion").list());
		for (Actualizacion ai : actTodas) {
			if (ai.getTicket().getId() == id) {
				actTicket.add(ai);
			}
		}

		s.getTransaction().commit();
		s.close();

		return actTicket;
	}

	@Override
	public Actualizacion createActualizacion(Actualizacion a) {
		Session session = SessionFactoryService.get().openSession();
		session.beginTransaction();

		try {
			session.save(a);
		} catch (Exception e) {
			a = null;
		}

		session.getTransaction().commit();
		session.close();

		return a;
	}

}
