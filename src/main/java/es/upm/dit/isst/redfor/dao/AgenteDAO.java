package es.upm.dit.isst.redfor.dao;

import java.util.List;

import es.upm.dit.isst.redfor.model.Agente;

public interface AgenteDAO {

	public List<Agente> readAgentes();

	public Agente createAgente(Agente a);

	public Agente readAgente(String email);

	public Agente updateAgente(Agente a);

}
