package es.upm.dit.isst.redfor.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import es.upm.dit.isst.redfor.model.Nodo;

public class NodoDAOImplementation implements NodoDAO {

	private static NodoDAOImplementation instancia = null;

	private NodoDAOImplementation() {
	}

	public static NodoDAOImplementation getInstance() {
		if (null == instancia)
			instancia = new NodoDAOImplementation();
		return instancia;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Nodo> readNodos() {
		List<Nodo> n = new ArrayList<Nodo>();
		Session s = SessionFactoryService.get().openSession();
		s.beginTransaction();
		n.addAll(s.createQuery("FROM Nodo").list());
		s.getTransaction().commit();
		s.close();
		return n;
	}

	@Override
	public Nodo createNodo(Nodo n) {
		Session session = SessionFactoryService.get().openSession();
		session.beginTransaction();
		try {
			session.save(n);
		} catch (Exception e) {
			n = null;
		}
		session.getTransaction().commit();
		session.close();
		return n;
	}

	@Override
	public Nodo readNodo(int id) {
		Session session = SessionFactoryService.get().openSession();
		session.beginTransaction();
		Nodo n = session.get(Nodo.class, id);
		session.getTransaction().commit();
		session.close();
		return n;
	}
	
}
