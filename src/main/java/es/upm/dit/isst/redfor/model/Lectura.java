package es.upm.dit.isst.redfor.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Lectura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int idNodo;
	private long timestamp;
	private double temperatura;
	private double viento;
	private double humedad;
	private boolean humo;
	private boolean bateriaBaja;

	public Lectura() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdNodo() {
		return idNodo;
	}

	public void setIdNodo(int idNodo) {
		this.idNodo = idNodo;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public double getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(double temperatura) {
		this.temperatura = temperatura;
	}

	public double getViento() {
		return viento;
	}

	public void setViento(double viento) {
		this.viento = viento;
	}

	public double getHumedad() {
		return humedad;
	}

	public void setHumedad(double humedad) {
		this.humedad = humedad;
	}

	public boolean isHumo() {
		return humo;
	}

	public void setHumo(boolean humo) {
		this.humo = humo;
	}

	public boolean isBateriaBaja() {
		return bateriaBaja;
	}

	public void setBateriaBaja(boolean bateriaBaja) {
		this.bateriaBaja = bateriaBaja;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lectura other = (Lectura) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Lectura [id=" + id + ", idNodo=" + idNodo + ", timestamp=" + timestamp + ", temperatura=" + temperatura
				+ ", viento=" + viento + ", humedad=" + humedad + ", humo=" + humo + ", bateriaBaja=" + bateriaBaja
				+ "]";
	}
	
}