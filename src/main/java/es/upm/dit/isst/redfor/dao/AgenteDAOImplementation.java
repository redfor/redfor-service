package es.upm.dit.isst.redfor.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import es.upm.dit.isst.redfor.model.Agente;

public class AgenteDAOImplementation implements AgenteDAO {

	private static AgenteDAOImplementation instancia = null;

	private AgenteDAOImplementation() {
	}

	public static AgenteDAOImplementation getInstance() {
		if (null == instancia)
			instancia = new AgenteDAOImplementation();
		return instancia;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Agente> readAgentes() {
		List<Agente> a = new ArrayList<Agente>();
		Session s = SessionFactoryService.get().openSession();
		s.beginTransaction();
		a.addAll(s.createQuery("FROM Agente").list());
		s.getTransaction().commit();
		s.close();
		return a;
	}

	@Override
	public Agente createAgente(Agente a) {
		Session session = SessionFactoryService.get().openSession();
		session.beginTransaction();
		try {
			session.save(a);
		} catch (Exception e) {
			a = null;
		}
		session.getTransaction().commit();
		session.close();
		return a;
	}

	@Override
	public Agente readAgente(String email) {
		Session session = SessionFactoryService.get().openSession();
		session.beginTransaction();
		Agente a = session.get(Agente.class, email);
		session.getTransaction().commit();
		session.close();
		return a;
	}

	@Override
	public Agente updateAgente(Agente a) {
		Session session = SessionFactoryService.get().openSession();
		session.beginTransaction();
		session.saveOrUpdate(a);
		session.getTransaction().commit();
		session.close();
		return a;
	}

}